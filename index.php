<?php
//session_start(); // we'll use sessions to securing resources, although I'm not sure we should

// We need to discuss URL rewriting before we do this

include_once("includes/config.inc.php");
include_once("includes/Router.inc.php");

// Get the requested url path (thanks to the magic of mod_rewrite)
// Note that the .htaccess file is configured to redirect to this page
// and append the requested path to the query string, for example: index.php?url_path=users/1
$url_path = $_GET['url_path'] ?? "";
die("REQUEST METHOD: " . $_SERVER['REQUEST_METHOD'] . "<br>URL:" . $url_path);


?>