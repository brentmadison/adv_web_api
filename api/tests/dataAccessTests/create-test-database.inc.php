<?php

// In phpMyAdmin, create a database named test_db
// and set the collation to: utf8mb4_unicode_ci

$testDB = "wtc";
$testServer = "localhost";
$testLogin = "root";
// NOTE that the MySQL password on the Ubuntu machine is 'test'
// AND the password on XAMPP is an empty string
// We can check the operating system of the server like so:
//die(PHP_OS); // PHP_OS is a predefined constant, it will be WINNT for Windows (XAMPP)
$testPassword = PHP_OS == "WINNT" ? "" : "test";

$link = mysqli_connect($testServer, $testLogin, $testPassword, $testDB);

if (!$link) {
    die("Unable to connect to test db");
}

$dropUsersTable = "DROP TABLE IF EXISTS users;";
$dropUserRolesTable = "DROP TABLE IF EXISTS user_roles;";
$dropInventory = "DROP TABLE IF EXISTS inventory;";
$dropSupplier = "DROP TABLE IF EXISTS supplier;";
$dropWarehouse = "DROP TABLE IF EXISTS warehouse;";

$createUsersTable = "
	CREATE TABLE IF NOT EXISTS users (
	  user_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	  user_first_name varchar(30) NOT NULL,
	  user_last_name varchar(30) NOT NULL,
	  user_email varchar(100) NOT NULL UNIQUE,
	  user_password char(255) NOT NULL,
	  user_salt char(32) NOT NULL,
	  user_role INT NOT NULL DEFAULT '1',
	  user_active enum('yes','no') NOT NULL DEFAULT 'yes'
	);";

$createUserRolesTable = "
	CREATE TABLE `user_roles` (
	  `user_role_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	  `user_role_name` varchar(30) NOT NULL,
	  `user_role_desc` varchar(200) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$createInventory = "CREATE TABLE `inventory` (
		`id` int(11) NOT NULL,
		`date_dropped_off` datetime NOT NULL DEFAULT current_timestamp(),
		`cost_per_day` int(11) NOT NULL DEFAULT 0,
		`type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
		`amount_crates` int(11) NOT NULL DEFAULT 0,
		`supplier_id` int(11) NOT NULL,
		`warehouse_id` int(11) NOT NULL
	  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

$createSupplier = "CREATE TABLE `supplier` (
	`id` int(11) NOT NULL,
	`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

$createWarehouse = "CREATE TABLE `warehouse` (
	`id` int(11) NOT NULL,
	`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`max_crates` int(11) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";

$populateUserRolesTable = "
	INSERT INTO `user_roles` (`user_role_id`, `user_role_name`, `user_role_desc`) VALUES
	(1, 'Standard User', 'Normal user with no special permissions'),
	(2, 'Admin', 'Extra permissions');";

$populateUsersTable = "
	INSERT INTO users (user_first_name,user_last_name, user_email, user_password, user_salt, user_role, user_active) VALUES
	('John', 'Doe','john@doe.com', 'opensesame', 'xxx', '1', 'yes'),
	('Jane', 'Anderson','jane@doe.com', 'letmein', 'xxx', '2', 'yes'),
	('Bob', 'Smith','bob@smith.com', 'test', 'xxx', '2', 'yes');";

$populateInventory = "INSERT INTO `inventory` (`id`, `date_dropped_off`, `cost_per_day`, `type`, `amount_crates`, `supplier_id`, `warehouse_id`) VALUES
(5, '2022-02-28 10:24:31', 11, 'FOOD', 12, 1, 1),
(6, '2022-02-28 10:24:53', 15, 'FOOD', 22, 2, 1),
(7, '2022-02-28 10:25:12', 421, 'MILK', 77, 3, 1);";
$populateSupplier = "INSERT INTO `supplier` (`id`, `name`, `email`, `location`) VALUES
(1, 'BRENT\'S MILK', 'brent@milk.com', 'Madison'),
(2, 'GOOD FOOD', 'great@food.com', 'California'),
(3, 'HOME MADE FOOD', 'food@food.com', 'La crosse');";
$populateWarehouse = "INSERT INTO `warehouse` (`id`, `name`, `location`, `max_crates`) VALUES
(1, 'BILLS WAREHOUSe', 'VERONA', 50012),
(2, 'NEDS HOUSING', 'LAKE GOONA', 24111),
(3, 'FREE SHIPPING WAREHOUSE', 'FREEVILLE', 1111);";

// Note: you have to drop the user_roles table before the users table
// because of the FK relationship
// You also have to create and populate the user_roles table before the users table
mysqli_query($link, $dropUserRolesTable) or die(mysqli_error($link));
mysqli_query($link, $dropUsersTable) or die(mysqli_error($link));
mysqli_query($link, $dropInventory) or die(mysqli_error($link));
mysqli_query($link, $dropSupplier) or die(mysqli_error($link));
mysqli_query($link, $dropWarehouse) or die(mysqli_error($link));

mysqli_query($link, $createUserRolesTable) or die(mysqli_error($link));
mysqli_query($link, $populateUserRolesTable) or die(mysqli_error($link));
mysqli_query($link, $createUsersTable) or die(mysqli_error($link));
mysqli_query($link, $populateUsersTable) or die(mysqli_error($link));

mysqli_query($link, $createInventory) or die(mysqli_error($link));
mysqli_query($link, $createSupplier) or die(mysqli_error($link));
mysqli_query($link, $createWarehouse) or die(mysqli_error($link));

mysqli_query($link, $populateInventory) or die(mysqli_error($link));
mysqli_query($link, $populateSupplier) or die(mysqli_error($link));
mysqli_query($link, $populateWarehouse) or die(mysqli_error($link));
