<?php
include_once "../../includes/dataaccess/InventoryDataAccess.inc.php";
include_once "../../includes/models/Inventory.inc.php";
include_once "create-test-database.inc.php";

$testResults = array();
$testOptions = array();

// You'll have to run all these tests for each of your data access classes
//testConstructor(); // we should test the constructor, but maybe we'll skip this one in the interest of time
testConvertModelToRow();
testConvertRowToModel();
testGetAll();
testGetById();
testInsert();
testUpdate();
testDelete();

echo (implode("<br>", $testResults));

function testConstructor()
{

    global $testResults, $link;
    $testResults[] = "<b>TESTING constructor...</b>";

    // TEST - create an instance of the ConcactDataAccess class
    $da = new InventoryDataAccess($link);

    if ($da) {
        $testResults[] = "PASS - Created instance of InventoryDataAccess";
    } else {
        $testResults[] = "FAIL - DID NOT creat instance of InventoryDataAccess";
    }

    // Test - an exception should be thrown if the $link param is not a valid link
    try {
        $da = new InventoryDataAccess("BLAHHHHHH");
        $testResults[] = "FAIL - Exception is NOT thrown when link param is invalid";
    } catch (Exception $e) {
        $testResults[] = "PASS - Exception is thrown when link param is invalid";
    }
}

function testConvertModelToRow()
{
    global $testResults, $link, $testOptions;

    $testOptions = array(
        'id' => 1,
        'date_dropped_off' => "12/12/2021",
        'cost_per_day' => 5,
        'type' => "supplies",
        'amount_crates' => 5,
        'supplier_id' => 1,
        'warehouse_id' => 1,
    );

    $testResults[] = "<b>TESTING convertModelToRow()...</b>";

    $da = new InventoryDataAccess($link);

    $r = new Inventory($testOptions);

    $actualResult = $da->convertModelToRow($r);

    if (empty(array_diff_assoc($testOptions, $actualResult))) {
        $testResults[] = "PASS - Converted Role to proper assoc array";
    } else {
        $testResults[] = "FAIL - DID NOT convert Role to proper assoc array";
    }
}

function testConvertRowToModel()
{
    global $testResults, $link, $testOptions;


    $testResults[] = "<b>TESTING convertRowToModel()...</b>";

    $da = new InventoryDataAccess($link);

    $actualResult = $da->convertRowToModel($testOptions);

    $expectedResult = new Inventory($testOptions);

    if ($actualResult->equals($expectedResult)) {
        $testResults[] = "PASS - Converted row (assoc array) to Role";
    } else {
        $testResults[] = "FAIL - DID NOT Convert row (assoc array) to Role";
    }
}

function testGetAll()
{
    global $testResults, $link;
    $testResults[] = "<b>TESTING getAll()...</b>";

    $da = new InventoryDataAccess($link);
    $actualResult = $da->getAll();

    //$testResults[] = print_r($actualResult, true);
    //var_dump($actualResult);die();
}

function testGetById()
{
    global $testResults, $link;
    $testResults[] = "<b>TESTING getById()...</b>";

    // We need an instance of a UserDataAccess object so that we can call the method we want to test
    $da = new InventoryDataAccess($link);
    $actualResult = $da->getById(1);

    //var_dump($actualResult);die();

}

function testInsert()
{
    global $testResults, $link, $testOptions;
    $testResults[] = "<b>TESTING insert()...</b>";

    $da = new InventoryDataAccess($link);


    $actualResult = $da->insert(new Inventory($testOptions));
    //var_dump($actualResult);die();
}

function testUpdate()
{

    global $testResults, $link, $testOptions;
    $testResults[] = "<b>TESTING update()...</b>";

    $da = new InventoryDataAccess($link);

    $actualResult = $da->update(new Inventory($testOptions));
    //var_dump($actualResult);die();

}

function testDelete()
{
    // Note sure how we want to handle this
    // If you allow deletes then it can get messy with FK relationships
    // It might be better to set active = no
}
