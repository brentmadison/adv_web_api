<?php
include_once("../../includes/dataaccess/UserDataAccess.inc.php");
include_once("../../includes/models/User.inc.php");
include_once("create-test-database.inc.php");


$testResults = array();

// You'll have to run all these tests for each of your data access classes
testConstructor();
testConvertModelToRow();
testConvertRowToModel();
testGetAll();
testGetById();
testInsert(); 
testUpdate(); 
testDelete(); 

echo(implode("<br>", $testResults));



function testConstructor(){

	global $testResults, $link;
	$testResults[] = "<b>TESTING constructor...</b>";

	// TEST - create an instance of the ConcactDataAccess class
	$da = new UserDataAccess($link);
	
	if($da){
		$testResults[] = "PASS - Created instance of UserDataAccess";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of UserDataAccess";
	}

	// Test - an exception should be thrown if the $link param is not a valid link
	try{
		$da = new UserDataAccess("BLAHHHHHH");
		$testResults[] = "FAIL - Exception is NOT thrown when link param is invalid";
	}catch(Exception $e){
		$testResults[] = "PASS - Exception is thrown when link param is invalid";
	}
}

function testConvertModelToRow(){
	global $testResults, $link;

	$testResults[] = "<b>TESTING convertModelToRow()...</b>";

	$da = new UserDataAccess($link);

	$options = array(
		'id' => 1,
		'firstName' => "Bob",
		'lastName' => "Smith",
		'email' => "bob@smith.com",
		'roleId' => "1",
		'password' => "opensesame",
		'salt' => "xxx",
		'active' => "yes"
	);

	$u = new User($options);

	$expectedResult = array(
		'user_id' => 1,
		'user_first_name' => "Bob",
		'user_last_name' => "Smith",
		'user_email' => "bob@smith.com",
		'user_role' => "1",
		'user_password' => "opensesame",
		'user_salt' => "xxx",
		'user_active' => "yes"
	);
	
	$actualResult = $da->convertModelToRow($u);

	if(empty(array_diff_assoc($expectedResult, $actualResult))){
		$testResults[] = "PASS - Converted User to proper assoc array";
	}else{
		$testResults[] = "FAIL - DID NOT convert User to proper assoc array";
	}
}


function testConvertRowToModel(){
	global $testResults, $link;

	$testResults[] = "<b>TESTING convertRowToModel()...</b>";

	$da = new UserDataAccess($link);

	$row = array(
		'user_id' => 1,
		'user_first_name' => "Bob",
		'user_last_name' => "Smith",
		'user_email' => "bob@smith.com",
		'user_role' => "1",
		'user_password' => "opensesame",
		'user_salt' => "xxx",
		'user_active' => "yes"
	);
	
	$actualUser = $da->convertRowToModel($row);
	
	$expectedUser = new User([
		'id' => 1,
		'firstName' => "Bob",
		'lastName' => "Smith",
		'email' => "bob@smith.com",
		'roleId' => "1",
		'password' => "opensesame",
		'salt' => "xxx",
		'active' => "yes"
	]);

	if($actualUser->equals($expectedUser)){
		$testResults[] = "PASS - Converted row (assoc array) to User";
	}else{
		$testResults[] = "FAIL - DID NOT Convert row (assoc array) to User";
	}
}


function testGetAll(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING getAll()...</b>";

	$da = new UserDataAccess($link);
	$actualResult = $da->getAll();
	// var_dump($actualResult); die();
}


function testGetById(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING getById()...</b>";

	// We need an instance of a UserDataAccess object so that we can call the method we want to test
	$da = new UserDataAccess($link);
	$actualResult = $da->getById(1);
	//var_dump($actualResult); die();
}

function testInsert(){
	global $testResults, $link;
	$testResults[] = "<b>TESTING insert()...</b>";

	$da = new UserDataAccess($link);

	$options = array(
		'id' => 1,
		'firstName' => "Bob",
		'lastName' => "Smith",
		'email' => "bobxxxx@smith.com", //email must be unique
		'roleId' => "1",
		'password' => "opensesame",
		'salt' => "xxx",
		'active' => "yes"
	);

	$u = new User($options);

	$actualResult = $da->insert($u);
	//var_dump($actualResult); die();

	/*
	// The insert method should throw an error if you try to 
	// insert a user_email that already exists in the database
	try{
		$newUser = $da->insert($u);
		die("FAIL - Did not throw error when user with duplicate email is inserted");
	}catch(Exception $e){
		die("PASS - Here's the exception that was thrown: " . $e->getMessage());
	}
	*/
	
}

function testUpdate(){

	global $testResults, $link;
	$testResults[] = "<b>TESTING update()...</b>";

	$da = new UserDataAccess($link);

	$options = array(
		'id' => 1,
		'firstName' => "Bobssssss",
		'lastName' => "Smith",
		'email' => "bobxxxxssssss@smith.com", //email must be unique
		'roleId' => "1",
		'password' => "opensesame",
		'salt' => "xxx",
		'active' => "yes"
	);

	$u = new User($options);

	$actualResult = $da->update($u);
	//var_dump($actualResult);die();

	/*
	// The update method should throw an error if a different user has the same email as the one being updated.
	$u->email = "jane@doe.com";
	try{
		$da->update($u);
		die("FAIL - Did not throw error when user with duplicate email is updated");
	}catch(Exception $e){
		die("PASS - Here's the exception that was thrown: " . $e->getMessage());
	}
	*/

}

function testDelete(){
	// Note sure how we want to handle this
	// If you allow deletes then it can get messy with FK relationships
	// It might be better to set active = no
}


?>
