<?php
include_once("../../includes/models/Inventory.inc.php");


// we'll use these options to create valid User in our tests
$options = array(
	'id' => 1,
	'date_dropped_off' => "12/12/2021",
	'cost_per_day' => 5,
	'type' => "supplies",
	'amount_crates' => 5,
	'supplier_id' => 1,
	'warehouse_id' => 1,
);

// This array will store the test results
$testResults = array();

// run the test functions
testConstructor();
testIsValid();

// display the results
echo(implode("<br>",$testResults));


function testConstructor(){
	global $testResults, $options;
	$testResults[] = "<b>Testing the constructor...</b>";

	$u = new Inventory();
	
	if($u){
		$testResults[] = "PASS - Created instance of User model object";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of a User model object";
	}

	$u = new Inventory($options);

	if($u->id == 1){
		$testResults[] = "PASS - Set id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set id properly";
	}
}


function testIsValid(){
	global $testResults, $options;
	$testResults[] = "<b>Testing isValid()...</b>";
		
	$u = new Inventory($options);
	$u->id = "asfa";

	if($u->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is not numeric";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is not numeric";
	}
}