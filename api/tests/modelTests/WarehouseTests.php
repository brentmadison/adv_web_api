<?php
include_once("../../includes/models/Warehouse.inc.php");


// we'll use these options to create valid User in our tests
$options = array(
	'id' => 1,
	'name' => "test",
	'location' => "test location",
	'max_crates' => 15,
);

// This array will store the test results
$testResults = array();

// run the test functions
testConstructor();
testIsValid();

// display the results
echo(implode("<br>",$testResults));


function testConstructor(){
	global $testResults, $options;
	$testResults[] = "<b>Testing the constructor...</b>";

	$u = new Warehouse();
	
	if($u){
		$testResults[] = "PASS - Created instance of User model object";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of a User model object";
	}

	$u = new Warehouse($options);

	if($u->id === 1){
		$testResults[] = "PASS - Set id properly";
	}else{
		$testResults[] = "FAIL - DID NOT set id properly";
	}
}


function testIsValid(){
	global $testResults, $options;
	$testResults[] = "<b>Testing isValid()...</b>";
		
	$u = new Warehouse($options);
	$u->id = "afafa";

	if($u->isValid() === false){
		$testResults[] = "PASS - isValid() returns false when ID is not numeric";
	}else{
		$testResults[] = "FAIL - isValid() DOES NOT return false when ID is not numeric";
	}
}





