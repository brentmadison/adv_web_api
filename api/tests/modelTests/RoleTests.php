<?php

include_once "../../includes/models/Role.inc.php";

// we'll use these options to create valid User in our tests

$options = array(

    'id' => 1,

    'name' => "test",

    'description' => "testinggggggg",

);

// This array will store the test results

$testResults = array();

// run the test functions

testConstructor();

testIsValid();

// display the results

echo (implode("<br>", $testResults));

function testConstructor()
{

    global $testResults, $options;

    $testResults[] = "<b>Testing the constructor...</b>";

    $u = new Role();

    if ($u) {

        $testResults[] = "PASS - Created instance of Role model object";
    } else {

        $testResults[] = "FAIL - DID NOT creat instance of a User model object";
    }

    // TEST - Make sure the firstName property gets set correctly

    $u = new Role($options);

    if ($u->id === 1) {

        $testResults[] = "PASS - Set id properly";
    } else {

        $testResults[] = "FAIL - DID NOT set id properly";
    }

    if ($u->name == "test") {

        $testResults[] = "PASS - Set name properly";
    } else {

        $testResults[] = "FAIL - DID NOT set name properly";
    }

    if ($u->description == "testinggggggg") {

        $testResults[] = "PASS - Set description properly";
    } else {

        $testResults[] = "FAIL - DID NOT set description properly";
    }
}

function testIsValid()
{

    global $testResults, $options;

    $testResults[] = "<b>Testing isValid()...</b>";

    // isValid() should return false when the ID is not numeric

    $u = new Role($options);

    $u->id = "";

    if ($u->isValid() === false) {

        $testResults[] = "PASS - isValid() returns false when ID is not numeric";
    } else {

        $testResults[] = "FAIL - isValid() DOES NOT return false when ID is not numeric";
    }

    // isValid() should return false when the firstName is empty

    $u = new Role($options);

    $u->name = "";

    if ($u->isValid() === false) {

        $testResults[] = "PASS - isValid() returns false when name is empty";
    } else {

        $testResults[] = "FAIL - isValid() DOES NOT return false when name is empty";
    }

    $u = new Role($options);

    $u->description = "";

    if ($u->isValid() === false) {

        $testResults[] = "PASS - isValid() returns false when description is empty";
    } else {

        $testResults[] = "FAIL - isValid() DOES NOT return false when description is empty";
    }
}
