<?php
include_once("Model.inc.php");

class Warehouse extends Model
{
	public $id;
	public $name;
	public $location;
	public $max_crates;

	public function __construct($args = [])
	{
		$this->id = $args['id'] ?? 0;
		$this->name = $args['name'] ?? "";
		$this->location = $args['location'] ?? "";
		$this->max_crates = $args['max_crates'] ?? 0;
	}

	public function isValid()
	{

		$valid = true;
		$this->validationErrors = [];

		if (!is_numeric($this->id)) {
			$valid = false;
			$this->validationErrors['id'] = "ID is not valid";
		}

		if (empty($this->name)) {
			$valid = false;
			$this->validationErrors['name'] = "name is required";
		} else if (strlen($this->name) > 100) {
			$valid = false;
			$this->validationErrors['name'] = "name must be 100 characters or less";
		}

		if (empty($this->location)) {
			$valid = false;
			$this->validationErrors['location'] = "location is required";
		} else if (strlen($this->location) > 100) {
			$valid = false;
			$this->validationErrors['location'] = "location must be 100 characters or less";
		}

		if (!is_numeric($this->max_crates) || $this->max_crates < 0) {
			$valid = false;
			$this->validationErrors['amount_crates'] = "amount_crates is not valid";
		}

		return $valid;
	}
}
