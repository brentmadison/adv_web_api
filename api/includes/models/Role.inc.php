<?php
include_once("Model.inc.php");

class Role extends Model
{
	public $id;
	public $name;
	public $description;
	
	public function __construct($args = []){
		$this->id = $args['id'] ?? 0;
		$this->name = $args['name'] ?? "";
		$this->description = $args['description'] ?? "";
	}

	public function isValid()
	{
		
		$valid = true;
		$this->validationErrors = [];
		
		if (!is_numeric($this->id)) {
			$valid = false;
			$this->validationErrors['id'] = "ID is not valid";
		}

		if (empty($this->name)) {
			$valid = false;
			$this->validationErrors['name'] = "Name is required";
		}else if(strlen($this->name) > 30){
			$valid = false;
			$this->validationErrors['name'] = "Name must be 30 characters or less";
		}

		if (empty($this->description)) {
			$valid = false;
			$this->validationErrors['description'] = "description is required";
		}else if(strlen($this->description) > 255){
			$valid = false;
			$this->validationErrors['description'] = "description must be 255 characters or less";
		}
		

		return $valid;
	}

}
