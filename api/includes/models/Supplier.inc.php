<?php
include_once("Model.inc.php");

class Supplier extends Model
{
	public $id;
	public $name;
	public $email;
	public $location;

	public function __construct($args = [])
	{
		$this->id = $args['id'] ?? 0;
		$this->name = $args['name'] ?? "";
		$this->email = $args['email'] ?? "";
		$this->location = $args['location'] ?? "";
	}

	public function isValid()
	{

		$valid = true;
		$this->validationErrors = [];

		if (!is_numeric($this->id)) {
			$valid = false;
			$this->validationErrors['id'] = "ID is not valid";
		}

		if (empty($this->name)) {
			$valid = false;
			$this->validationErrors['name'] = "name is required";
		} else if (strlen($this->name) > 30) {
			$valid = false;
			$this->validationErrors['name'] = "name must be 30 characters or less";
		}

		if (empty($this->email)) {
			$valid = false;
			$this->validationErrors['email'] = "Email is required";
		} else if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			$valid = false;
			$this->validationErrors['email'] = "The email address is not valid";
		} else if (strlen($this->email) > 100) {
			$valid = false;
			$this->validationErrors['email'] = "Email must be 100 characters or less";
		}

		if (empty($this->location)) {
			$valid = false;
			$this->validationErrors['location'] = "location is required";
		} else if (strlen($this->location) > 100) {
			$valid = false;
			$this->validationErrors['location'] = "location must be 100 characters or less";
		}

		return $valid;
	}
}
