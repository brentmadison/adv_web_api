<?php
include_once("Model.inc.php");

class Inventory extends Model
{

	// INSTANCE VARIABLES
	public $id;
	public $date_dropped_off;
	public $cost_per_day;
	public $type;
	public $amount_crates;
	public $supplier_id;
	public $warehouse_id;
	
	
	public function __construct($args = []){
		$this->id = $args['id'] ?? 0;
		$this->date_dropped_off = $args['date_dropped_off'] ?? 0;
		$this->cost_per_day = $args['cost_per_day'] ?? 0;
		$this->type = $args['type'] ?? "N/A";
		$this->amount_crates = $args['amount_crates'] ?? 0;
		$this->supplier_id = $args['supplier_id'] ?? -1;
		$this->warehouse_id = $args['warehouse_id'] ?? -1;
	}

	public function isValid()
	{
		
		$valid = true;
		$this->validationErrors = [];

		if (!is_numeric($this->id)) {
			$valid = false;
			$this->validationErrors['id'] = "ID is not valid";
		}

		if (!is_numeric($this->cost_per_day) || $this->cost_per_day < 0) {
			$valid = false;
			$this->validationErrors['cost_per_day'] = "cost_per_day is not valid";
		}

		if (empty($this->type)) {
			$valid = false;
			$this->validationErrors['type'] = "type is required";
		}else if(strlen($this->type) > 30){
			$valid = false;
			$this->validationErrors['type'] = "type must be 30 characters or less";
		}

		if (!is_numeric($this->amount_crates) || $this->amount_crates < 0) {
			$valid = false;
			$this->validationErrors['amount_crates'] = "amount_crates is not valid";
		}
		
		return $valid;
	}

}
