<?php

require_once "DataAccess.inc.php";
include_once "../../includes/models/Warehouse.inc.php";

class WarehouseDataAccess extends DataAccess
{

    public function __construct($link)
    {
        parent::__construct($link);
    }

    public function convertModelToRow($object)
    {

        $row = [];
        $row['id'] = mysqli_real_escape_string($this->link, $object->id);
        $row['name'] = mysqli_real_escape_string($this->link, $object->name);
        $row['max_crates'] = mysqli_real_escape_string($this->link, $object->max_crates);
        $row['location'] = mysqli_real_escape_string($this->link, $object->location);

        return $row;
    }

    public function convertRowToModel($row)
    {

        $u = new Warehouse();
        $u->id = htmlentities($row['id']);
        $u->name = htmlentities($row['name']);
        $u->max_crates = htmlentities($row['max_crates']);
        $u->location = htmlentities($row['location']);

        return $u;
    }
    public function getAll($args = null)
    {

        $qStr = "SELECT
		           *
		        FROM warehouse";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        $allUsers = array();

        while ($row = mysqli_fetch_assoc($result)) {
            $object = $this->convertRowToModel($row);
            $allUsers[] = $object;
        }

        return $allUsers;
    }

    public function getById($id)
    {

        $qStr = "SELECT
		            *
		        FROM warehouse
		        WHERE id = " . mysqli_real_escape_string($this->link, $id);

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        if ($result->num_rows == 1) {

            $row = mysqli_fetch_assoc($result);
            $object = $this->convertRowToModel($row);
            return $object;
        }

        return false;
    }

    public function insert($object)
    {

        $row = $this->convertModelToRow($object);

        $qStr = "INSERT INTO warehouse (
                    name,
                    max_crates,
                    location

                ) VALUES (
                    '{$row['name']}',
                    '{$row['max_crates']}',
                    '{$row['location']}'
                )";

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        if ($result) {
            // add the user id that was assigned by the data base
            $object->id = mysqli_insert_id($this->link);
            return $object;
        } else {
            $this->handleError("unable to insert user");
        }

        return false;
    }

    /**
     * Updates a user in the database
     * @param {User} $object
     * @return {boolean}                Returns true if the update succeeds
     */
    public function update($object)
    {

        // NOTE: we'll run into lots of complex problems to solve when we
        // we start salting and hashing passwords

        $row = $this->convertModelToRow($object);

        $qStr = "UPDATE warehouse SET
				name = '{$row['name']}',
				max_crates = '{$row['max_crates']}',
				location = '{$row['location']}'
			WHERE id = " . $row['id'];

        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        if ($result) {
            return true;
        } else {
            $this->handleError("unable to update user");
        }

        return false;
    }

    /**
     * Deletes a row from a table in the database
     * @param {number}     The id of the row to delete
     * @return {boolean}    Returns true if the row was sucessfully deleted, false otherwise
     */
    public function delete($args = null)
    {
        // Note sure how we want to handle this
        // If you allow deletes then it can get messy with FK relationships
        // It might be better to set active = no
    }

    // Note - we'll add methods for authenticating users and handling passwords

}
