<?php


require_once("DataAccess.inc.php");
include_once("../../includes/models/Inventory.inc.php");


class InventoryDataAccess extends DataAccess{

	function __construct($link){
		parent::__construct($link);
	}

	function convertModelToRow($object){

		$row = [];
		$row['id']	= mysqli_real_escape_string($this->link, $object->id);
		$row['date_dropped_off'] = mysqli_real_escape_string($this->link, $object->date_dropped_off);
		$row['cost_per_day'] = mysqli_real_escape_string($this->link, $object->cost_per_day);
		$row['type'] = mysqli_real_escape_string($this->link, $object->type);
		$row['amount_crates'] = mysqli_real_escape_string($this->link, $object->amount_crates);
		$row['supplier_id'] = mysqli_real_escape_string($this->link, $object->supplier_id);
		$row['warehouse_id'] = mysqli_real_escape_string($this->link, $object->warehouse_id);

		return $row;
	}

	function convertRowToModel($row){

		$u = new Inventory();
		$u->id = htmlentities($row['id']);
		$u->date_dropped_off = htmlentities($row['date_dropped_off']);
		$u->cost_per_day = htmlentities($row['cost_per_day']);
		$u->type = htmlentities($row['type']);
		$u->amount_crates = htmlentities($row['amount_crates']);
		$u->supplier_id = htmlentities($row['supplier_id']);
		$u->warehouse_id = htmlentities($row['warehouse_id']);


		return $u;
	}
	function getAll($args = null){
		
		$qStr = "SELECT
		           *
		        FROM inventory";
		    

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));
		
		$allUsers = array();

		while($row = mysqli_fetch_assoc($result)){
		    $object = $this->convertRowToModel($row);
		    $allUsers[] = $object;
		}

		return $allUsers;
	}


	function getById($id){
		
		$qStr = "SELECT
		            *
		        FROM inventory
		        WHERE id = " . mysqli_real_escape_string($this->link, $id);
		

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));
		
		if($result->num_rows == 1){

		    $row = mysqli_fetch_assoc($result);
		    $object = $this->convertRowToModel($row);
		    return $object;
		}

		return false;
	}

	

	function insert($object){
		        
        $row = $this->convertModelToRow($object);


        $qStr = "INSERT INTO inventory (
                    date_dropped_off, 
                    cost_per_day, 
                    type, 
                    amount_crates, 
                    supplier_id, 
                    warehouse_id
                ) VALUES (
                    '{$row['date_dropped_off']}',
                    '{$row['cost_per_day']}',
                    '{$row['type']}',
                    '{$row['amount_crates']}',
                    '{$row['supplier_id']}',
                    '{$row['warehouse_id']}'
                )";


        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        if($result){
            // add the user id that was assigned by the data base
            $object->id = mysqli_insert_id($this->link);
            return $object;
        }else{
            $this->handleError("unable to insert user");
        }

        return false;
	}

	/**
	 * Updates a user in the database
	 * @param {User} $object
	 * @return {boolean}				Returns true if the update succeeds			
	 */
	function update($object){
        
		// NOTE: we'll run into lots of complex problems to solve when we
		// we start salting and hashing passwords

        $row = $this->convertModelToRow($object);
               
        $qStr = "UPDATE inventory SET
				date_dropped_off = '{$row['date_dropped_off']}',
				cost_per_day = '{$row['cost_per_day']}',
				type = '{$row['type']}',
				amount_crates = '{$row['amount_crates']}',
				supplier_id = '{$row['supplier_id']}',
				warehouse_id = '{$row['warehouse_id']}'
			WHERE id = " . $row['id'];
       


		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

		if($result){
			return true;
		}else{
			$this->handleError("unable to update user");
		}

		return false;
	}


	/**
    * Deletes a row from a table in the database
    * @param {number} 	The id of the row to delete
    * @return {boolean}	Returns true if the row was sucessfully deleted, false otherwise
    */
	function delete($args = null){
		// Note sure how we want to handle this
		// If you allow deletes then it can get messy with FK relationships
		// It might be better to set active = no
	}

	
	// Note - we'll add methods for authenticating users and handling passwords

}