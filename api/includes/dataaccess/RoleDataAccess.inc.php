<?php


require_once("DataAccess.inc.php");
include_once("../../includes/models/Role.inc.php");


class RoleDataAccess extends DataAccess{

	
	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		parent::__construct($link);
	}

	function convertModelToRow($user){

		$row = [];
		$row['user_role_id']	= mysqli_real_escape_string($this->link, $user->id);
		$row['user_role_name'] = mysqli_real_escape_string($this->link, $user->name);
		$row['user_role_desc'] = mysqli_real_escape_string($this->link, $user->description);

		return $row;
	}

	function convertRowToModel($row){

		$u = new Role();
		$u->id = htmlentities($row['user_role_id']);
		$u->name = htmlentities($row['user_role_name']);
		$u->description = htmlentities($row['user_role_desc']);


		return $u;
	}
	function getAll($args = null){
		
		// Write a SQL query to select the id, first name, last name, and email address of every user in the users table
		$qStr = "SELECT
		            user_role_id, user_role_name, user_role_desc
		        FROM user_roles";
		    

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));
		
		$allUsers = array();

		while($row = mysqli_fetch_assoc($result)){
		    $user = $this->convertRowToModel($row);
		    $allUsers[] = $user;
		}

		return $allUsers;
	}


	function getById($id){
		
		$qStr = "SELECT
		            *
		        FROM user_roles
		        WHERE user_role_id = " . mysqli_real_escape_string($this->link, $id);
		

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));
		
		if($result->num_rows == 1){

		    $row = mysqli_fetch_assoc($result);
		    $user = $this->convertRowToModel($row);
		    return $user;
		}

		return false;
	}

	

	function insert($user){
		        
        $row = $this->convertModelToRow($user);


        $qStr = "INSERT INTO user_roles (
                    user_role_id,
                    user_role_name, 
                    user_role_desc
                ) VALUES (
                    '{$row['user_role_id']}',
                    '{$row['user_role_name']}',
                    '{$row['user_role_desc']}'
                )";


        $result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

        if($result){
            // add the user id that was assigned by the data base
            $user->id = mysqli_insert_id($this->link);
            return $user;
        }else{
            $this->handleError("unable to insert user");
        }

        return false;
	}

	/**
	 * Updates a user in the database
	 * @param {User} $user
	 * @return {boolean}				Returns true if the update succeeds			
	 */
	function update($user){
        
		// NOTE: we'll run into lots of complex problems to solve when we
		// we start salting and hashing passwords

        $row = $this->convertModelToRow($user);
               
        $qStr = "UPDATE user_roles SET
				user_role_name = '{$row['user_role_name']}',
				user_role_desc = '{$row['user_role_desc']}'
			WHERE user_role_id = " . $row['user_role_id'];
       

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

		if($result){
			return true;
		}else{
			$this->handleError("unable to update user");
		}

		return false;
	}


	/**
    * Deletes a row from a table in the database
    * @param {number} 	The id of the row to delete
    * @return {boolean}	Returns true if the row was sucessfully deleted, false otherwise
    */
	function delete($args = null){
		// Note sure how we want to handle this
		// If you allow deletes then it can get messy with FK relationships
		// It might be better to set active = no
	}

	
	// Note - we'll add methods for authenticating users and handling passwords

}